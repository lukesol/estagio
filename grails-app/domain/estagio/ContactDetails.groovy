package estagio

class ContactDetails {

    Integer id
    String mobile
    String phone
    String email
    String website
    String address
    String type
    Contact contact

    Date dateCreated
    Date lastUpdated


    static constraints = {
        mobile(unique:true, blank: false)
        phone(nullable: true, blank: true)
        email(unique:true, blank: false)
        website(nullable: true, blank: true)
        address(unique:true, blank: false )
    }

    static mapping = {
        version(false)
    }
}